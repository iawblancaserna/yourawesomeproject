<?php

use App\Http\Controllers\ProductController;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;

use App\Http\Controllers\UserController;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// 1
Route::get('/products', function() {
    return new ProductCollection(Product::all());
});

// 2
Route::get('/products/{id}', function($id) {
    return new ProductResource(Product::find($id));
});

// 3
Route::post('/product', [ProductController::class, 'store']);

// 4
Route::patch('/product/{id}', [ProductController::class, 'update']);

// 5
Route::delete('/product/delete/{id}', [ProductController::class, 'delete']);

// 6
Route::post('/user', [UserController::class, 'create']);

// 7
Route::patch('/user/{id}', [UserController::class, 'update']);

// 8
Route::delete('/user/delete/{id}', [UserController::class, 'deleteUser']);

// Per veure que s'ha eliminat un user
Route::get('/users/{id}', function($id) {
    return new UserResource(User::find($id));
});


// 9


