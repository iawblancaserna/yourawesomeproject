<?php

namespace App\Http\Controllers;
use App\Models\User;

use Illuminate\Http\Request;

class UserController extends Controller
{
    //

    public function __construct() {
        $this->user = new User();
    }

    public function create(Request $request) {
        $user = User::create($request->all());
        return response()->json($user, 200);
    }

    public function update(Request $request, $id) {
        $user = User::find($id); 

        // Crea un nou producte amb tots els camps del formulari
        $user->update($request->all());
        // Retorna el json del prod i un codi d'error o si anat be (200)
        return response()->json($user, 200);
    }    

    public function deleteUser(Request $request, $id) {
        $user = User::find($id); 
        $user->delete($request->all());
        return response()->json($user, 200);
    }    

}
